// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "comment-formatting" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('comment-formatting.helloWorld', function () {
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		vscode.window.showInformationMessage('8=>');
	});
	context.subscriptions.push(disposable);

	let slt = vscode.commands.registerCommand(
		"comment-formatting.single_line_title",
		function() {
			// get the active editor
			const editor = vscode.window.activeTextEditor;
			if (!editor) {
				return;
			}

			// get the current line's text
			const line = editor.document.lineAt(editor.selection.active.line);
			const line_text = line.text;

			// determine the indentation and the text
			var [indent, text] = get_indent_and_text(line_text);

			// build the comment line
			var comment = indent + "// ";
			var dash_count = 80 - comment.length - text.length;
			if (dash_count > 0) {
				dash_count /= 2;
			}
			else {
				dash_count = 0;
			}
			// insert the first lot of dashes
			for (var i = 0; i < dash_count; i++) {
				comment += "-";
			}
			// insert the centered text
			comment += text;
			// insert the second lot of dashes until the end of the line
			for (var i = comment.length; i < 80; i++) {
				comment += "-";
			}

			// insert into the editor
			editor.edit(edit_builder => {
				edit_builder.delete(line.range);
				edit_builder.insert(editor.selection.active, comment);
			});
		}
	);
	context.subscriptions.push(slt);

	let mlt = vscode.commands.registerCommand(
		"comment-formatting.multi_line_title",
		function() {
			// get the active editor
			const editor = vscode.window.activeTextEditor;
			if (!editor) {
				return;
			}

			// get the current line's text
			const line = editor.document.lineAt(editor.selection.active.line);
			const line_text = line.text;

			// determine the indentation and the text
			var [indent, text] = get_indent_and_text(line_text);

			// build the ruler line
			var ruler = indent + "// ";
			for (var i = ruler.length; i < 80; i++) {
				ruler += "-";
			}
			// build the title line
			var title = indent + "// ";
			var offset = 80 - title.length - text.length;
			if (offset > 0) {
				offset /= 2;
			}
			else {
				offset = 0;
			}
			// insert the offset
			for (var i = 0; i < offset; i++) {
				title += " ";
			}
			// insert the centered text
			title += text;

			// insert into the editor
			editor.edit(edit_builder => {
				edit_builder.delete(line.range);
				edit_builder.insert(editor.selection.active, ruler + "\n");
				edit_builder.insert(editor.selection.active, title + "\n");
				edit_builder.insert(editor.selection.active, ruler);
			});
		}
	);
	context.subscriptions.push(mlt);

	let wt = vscode.commands.registerCommand(
		"comment-formatting.wide_text",
		function() {
			// get the active editor
			const editor = vscode.window.activeTextEditor;
			if (!editor) {
				return;
			}

			// get the current line's text
			const line = editor.document.lineAt(editor.selection.active.line);
			const line_text = line.text;

			// determine the indentation and the text
			var [indent, text] = get_indent_and_text(line_text);

			// build the new line
			var new_line = indent;
			for (var i = 0; i < text.length; i++) {
				new_line += text.charAt(i) + " ";
			}

			// insert into the editor
			editor.edit(edit_builder => {
				edit_builder.delete(line.range);
				edit_builder.insert(editor.selection.active, new_line);
			});
		}
	);
	context.subscriptions.push(wt);
}

function get_indent_and_text(line_text) {
	var fi = true;
	var indent = "";
	var text = "";
	for (var i = 0; i < line_text.length; i++) {
		var c = line_text.charAt(i);
		if (fi && c == " ") {
			indent += " ";
			continue;
		}
		fi = false;
		text += c;
	}
	text = text.trim();

	return [indent, text];
}

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}
